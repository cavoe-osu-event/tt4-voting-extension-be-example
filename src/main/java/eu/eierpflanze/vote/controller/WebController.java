package eu.eierpflanze.vote.controller;

import eu.eierpflanze.vote.Beatmap;
import eu.eierpflanze.vote.Vote;
import org.springframework.web.bind.annotation.*;

@RestController
public class WebController {

    private static Vote currentVote;

    @CrossOrigin(origins = "http://localhost:8000")
    @GetMapping("/vote")
    public Vote getVoteStatus(){
        return currentVote;
    }

    @CrossOrigin(origins = "http://localhost:8000")
    @PostMapping("/vote")
    public void voteForMap(@RequestParam(value = "mapId") Long mapId, @RequestParam(value = "userId") String userId){
        currentVote.voteForMap(mapId,userId);
    }

    @CrossOrigin(origins = "http://localhost:8000")
    @PostMapping("/start")
    public void startVote(){
        Beatmap map1 = new Beatmap(2220287L,1060431L,"Pastel Rain","Sangatsu no Phantasia","[Extra]","FM");
        Beatmap map2 = new Beatmap(2658939L,1280001L,"cold weather","glass beach","[extra]","DT");
        Beatmap map3 = new Beatmap(2608105L,1254930L,"You (=I)","BOL4","[Insane]","RX");
        Beatmap map4 = new Beatmap(2650207L,1275523L,"smooooch -Akiba Koubou mix- / Remixed by DJ Command","kors k","[move your body]","HD");
        currentVote = new Vote(30,map1,map2,map3,map4);
    }

    @CrossOrigin(origins = "http://localhost:8000")
    @PostMapping("/end")
    public void endVote(){
        currentVote.endVote();
    }

}
