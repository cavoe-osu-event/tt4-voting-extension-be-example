package eu.eierpflanze.vote;

import java.util.stream.IntStream;

public class Beatmap {
    public Long id;
    public Long setId;
    public String name;
    public String artist;
    public String difficulty;
    public String mapType;
    public Integer votePercent;
    public Integer voteOrder;
    public Integer votes;

    public Beatmap(Long id, Long setId, String name, String artist, String difficulty, String mapType) {
        this.id = id;
        this.setId = setId;
        this.name = name;
        this.artist = artist;
        this.difficulty = difficulty;
        this.mapType = mapType;
        this.votePercent = 0;
        this.voteOrder = 0;
        this.votes = 0;
    }
}
