package eu.eierpflanze.vote;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
public class Vote {

    public boolean voteRunning;
    public  List<Beatmap> maps;
    public int voteDurationSeconds;
    public LocalDateTime voteStart;

    @JsonIgnore
    Set<String> usersThatVoted;

    @JsonIgnore
    int totalVotes;

    //TODO don't sort after every vote
    public void voteForMap(Long mapId, String userId){
        if(!usersThatVoted.contains(userId) && voteRunning){

           maps.stream().filter(map -> map.id.equals(mapId)).findAny().ifPresent(map -> map.votes++);
           usersThatVoted.add(userId);
           totalVotes++;
           calculateVotes();
        }
    }

    public void endVote(){
        voteRunning = false;
    }

    public Vote(Beatmap... maps){
        totalVotes = 0;
        voteRunning = true;
        usersThatVoted = new HashSet<>();
        this.maps = new ArrayList<>();

        this.maps.addAll(Arrays.asList(maps));
    }

    public Vote(int voteDurationSeconds, Beatmap... maps){
        this(maps);
        this.voteDurationSeconds = voteDurationSeconds;
        this.voteStart = LocalDateTime.now(ZoneId.of("UTC"));
        new Timer().schedule(new TimerTask(){
            @Override
            public void run() {
                endVote();
            }
        },voteDurationSeconds * 1000);
    }

    private void calculateVotes(){

        maps.forEach(map -> map.votePercent = Math.round( ((float) map.votes / (float) totalVotes) * 100));

        //Make a copy so we don't sort the original list
        List<Beatmap> mapsCopy = new ArrayList<>();
        mapsCopy.addAll(maps);

        mapsCopy.sort(Comparator.comparingInt((Beatmap map) -> map.votePercent).reversed());
        for (int i = 0; i < maps.size(); i++) {
            mapsCopy.get(i).voteOrder = i;
        }
    }
}
